# andre_grid

Generate your own CSS grid-system.

## Setup

`yarn install`

Installs all NodeJS dependencies.

## Development

`yarn run watch`

Generates a CSS file for testing your changes to the grid.

## Build

`yarn run build`

Runs the build tasks.

Yes, its that simple! Just type in this commands and go on!
